# cemu_wii_u_helper_scripts

This repository houses Bash shell-scripts that help with Nintendo Wii U games for use with the Cemu Wii U emulator.

These scripts will copy the directories that house files associated with individual Nintendo Wii U games. These files are in an encrypted state, and will be decrypted using an application called CDecrypt, which is a Windows binary. Since I'm running from Debian GNU/Linux, Wine will be utilized in running this tool. CDecrypt will read in the encrypted files and output 3 new directories:

*  code
*  content
*  meta

After these directories and their associated unencrypted files are created, the original encrypted files are deleted, as they are no longer necessary.