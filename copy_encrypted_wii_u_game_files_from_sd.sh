#!/bin/bash

# Input Arguments
WII_U_GAME=${1}

# Environment Variables
SD_MOUNT_POINT="/media/alex/wii_u_sd"
ENCRYPTED_WII_U_GAME_DIR="${SD_MOUNT_POINT}/WUP-*"
MASS_STORAGE_DIR="/mnt/mass_storage/games/nintendo_wii_u/games"
LANDING_DIR="${MASS_STORAGE_DIR}/${WII_U_GAME}"

mv ${ENCRYPTED_WII_U_GAME_DIR} ${LANDING_DIR}

if [ ${?} -eq 0 ]
    then
        echo "Encrypted files for ${WII_U_GAME} successfully moved from ${ENCRYPTED_WII_U_GAME_DIR} to ${LANDING_DIR}."
else
	echo "Something went wrong..."
fi
